package com.anusit.week8;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5);
        rect1.printArea();

        Rectangle rect2 = new Rectangle(5, 3);
        rect2.printArea();

        Circle circle1 = new Circle(1);
        circle1.circleArea();

        Circle circle2 = new Circle(2);
        circle2.circleArea();

        Triangle triangle1 = new Triangle(5, 5, 6);
        triangle1.areaTriangle();

        Rectangle rect11 = new Rectangle(10, 5);
        rect11.printPerimeter();

        Rectangle rect22 = new Rectangle(5, 3);
        rect22.printPerimeter();

        Circle circle11 = new Circle(1);
        circle11.printPerimeter();

        Circle circle22 = new Circle(2);
        circle22.printPerimeter();

        Triangle triangle11 = new Triangle(5, 5, 6);
        triangle11.printPerimeter();
    }
}
